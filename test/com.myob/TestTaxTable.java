package com.myob;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class TestTaxTable {
    public static final BigDecimal ZERO = BigDecimal.ZERO;
    private static final List<BigDecimal> lowerRangeValues = Arrays.asList(
            ZERO,
            i_bigDecimal(18201),
            i_bigDecimal(37001),
            i_bigDecimal(87001),
            i_bigDecimal(180001));


    private static final List<BigDecimal> upperRangeValues = Arrays.asList(
            i_bigDecimal(18200),
            i_bigDecimal(37000),
            i_bigDecimal(87000),
            i_bigDecimal(180000),
            i_bigDecimal(Integer.MAX_VALUE));

    private static final List<BigDecimal> baseValues = Arrays.asList(
            ZERO,
            ZERO,
            i_bigDecimal(3572),
            i_bigDecimal(19822),
            i_bigDecimal(54232));

    private static final List<BigDecimal> plusValues = Arrays.asList(
            ZERO,
            d_bigDecimal(0.19),
            d_bigDecimal(0.325),
            d_bigDecimal(0.37),
            d_bigDecimal(0.45));

    private static final BigDecimal forEachDollar = BigDecimal.ONE;

    private static final List<BigDecimal> overValues = Arrays.asList(
            ZERO,
            i_bigDecimal(18200),
            i_bigDecimal(37000),
            i_bigDecimal(87000),
            i_bigDecimal(180000));

    public static final List<TaxEntry> taxTable = Arrays.asList(
            new TaxEntry(lowerRangeValues.get(0), upperRangeValues.get(0), baseValues.get(0), plusValues.get(0), forEachDollar, overValues.get(0)),
            new TaxEntry(lowerRangeValues.get(1), upperRangeValues.get(1), baseValues.get(1), plusValues.get(1), forEachDollar, overValues.get(1)),
            new TaxEntry(lowerRangeValues.get(2), upperRangeValues.get(2), baseValues.get(2), plusValues.get(2), forEachDollar, overValues.get(2)),
            new TaxEntry(lowerRangeValues.get(3), upperRangeValues.get(3), baseValues.get(3), plusValues.get(3), forEachDollar, overValues.get(3)),
            new TaxEntry(lowerRangeValues.get(4), upperRangeValues.get(4), baseValues.get(4), plusValues.get(4), forEachDollar, overValues.get(4))
    );

    private static BigDecimal i_bigDecimal(int value) {
        return new BigDecimal(value);
    }

    private static BigDecimal d_bigDecimal(double value) {
        return new BigDecimal(value);
    }
}
