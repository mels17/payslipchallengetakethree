package com.myob;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AppTest {

    private void assertPayslips(Payslip expected, Payslip actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getPayPeriod(), actual.getPayPeriod());
//        assertEquals(expected.getGrossIncome(), actual.getGrossIncome());
//        assertEquals(expected.getIncomeTax(), actual.getIncomeTax());
//        assertEquals(expected.getNetIncome(), actual.getNetIncome());
//        assertEquals(expected.getSuperAmount(), actual.getSuperAmount());
    }

    @Test
    public void givenEmployeeDetailsAndTaxTableReturnPayslip() {
        Payslip expectedPayslip = new Payslip("firstName lastName", "1 March - 31 March",
                TestTaxTable.ZERO, TestTaxTable.ZERO, TestTaxTable.ZERO, TestTaxTable.ZERO);

        BigDecimal annualSalary = TestTaxTable.ZERO;
        Employee employee = new Employee("firstName", "lastName", annualSalary);

        Payslip actualPayslip = employee.pay(TestTaxTable.taxTable, 0, "1 March", "31 March");

        assertPayslips(expectedPayslip, actualPayslip);
    }

    @Test
    public void givenEmployeeDetailsWithTaxCharged0ReturnPayslip() {
        Payslip expectedPayslip = new Payslip("Jane Doe", "1 October - 31 October",
                new BigDecimal(1000), TestTaxTable.ZERO, new BigDecimal(1000), new BigDecimal(120));

        BigDecimal annualSalary = new BigDecimal(12000);
        Employee employee = new Employee("Jane", "Doe", annualSalary);

        Payslip actualPayslip = employee.pay(TestTaxTable.taxTable, 12,
                "1 October", "31 October");

        assertPayslips(expectedPayslip, actualPayslip);
    }

    @Test
    public void givenEmployeeSalaryInSecondRangeReturnPayslipWithTaxDeductions() {
        Payslip expectedPayslip = new Payslip("John Doe", "1 January - 31 January",
                new BigDecimal(5004), new BigDecimal(922), new BigDecimal(4082), new BigDecimal(450));

        BigDecimal annualSalary = new BigDecimal(60050);
        Employee employee = new Employee("John", "Doe", annualSalary);

        Payslip actualPayslip = employee.pay(TestTaxTable.taxTable, 9,
                "1 January", "31 January");

        assertPayslips(expectedPayslip, actualPayslip);
    }
}
