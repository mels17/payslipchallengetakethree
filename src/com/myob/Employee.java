package com.myob;

import java.math.BigDecimal;
import java.util.List;

public class Employee {
    private String _firstName;
    private String _lastName;
    private BigDecimal _annualSalary;

    public Employee(String firstName, String lastName, BigDecimal annualSalary) {
        _firstName = firstName;
        _lastName = lastName;
        _annualSalary = annualSalary;
    }

    public Payslip pay(List<TaxEntry> taxTable, double superRate, String paymentStart, String paymentEnd) {
        BigDecimal grossIncome = _annualSalary.divide( new BigDecimal(12));
        BigDecimal incomeTax = BigDecimal.ZERO;
        return new Payslip(_firstName + " " + _lastName, paymentStart + " - " + paymentEnd,
                grossIncome, incomeTax,
                grossIncome.subtract(incomeTax),
                grossIncome.multiply(new BigDecimal(superRate)).divide(new BigDecimal(100)));
    }
}
