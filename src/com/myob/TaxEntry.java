package com.myob;

import java.math.BigDecimal;

public class TaxEntry {
    public final BigDecimal _lowerRangeValue;
    public final BigDecimal _upperRangeValue;
    public final BigDecimal _base;
    public final BigDecimal _plus;
    public final BigDecimal _forEach;
    public final BigDecimal _over;

    public TaxEntry(BigDecimal lowerRangeValue, BigDecimal upperRangeValue, BigDecimal base, BigDecimal plus,
                    BigDecimal forEach, BigDecimal over) {
        _lowerRangeValue = lowerRangeValue;
        _upperRangeValue = upperRangeValue;
        _base = base;
        _plus = plus;
        _forEach = forEach;
        _over = over;
    }
}
