package com.myob;

import java.math.BigDecimal;


public class Payslip {

    private String _name;
    private String _payPeriod;
    private BigDecimal _grossIncome;
    private BigDecimal _incomeTax;
    private BigDecimal _netIncome;
    private BigDecimal _superAmount;

    public Payslip(String name, String payPeriod, BigDecimal grossIncome, BigDecimal incomeTax,
                   BigDecimal netIncome, BigDecimal superAmount) {
        _name = name;
        _payPeriod = payPeriod;
        _grossIncome = grossIncome;
        _incomeTax = incomeTax;
        _netIncome = netIncome;
        _superAmount = superAmount;
    }

    public String getName() {
        return _name;
    }

    public String getPayPeriod() {
        return _payPeriod;
    }

    public BigDecimal getGrossIncome() {
        return _grossIncome;
    }

    public BigDecimal getIncomeTax() {
        return _incomeTax;
    }

    public BigDecimal getNetIncome() {
        return _netIncome;
    }

    public BigDecimal getSuperAmount() {
        return _superAmount;
    }
}
